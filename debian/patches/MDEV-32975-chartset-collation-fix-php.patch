Origin: https://github.com/MariaDB/server/commit/1b37cb71f44549c94acf8914cf93d43a4293a449 (merged in Feb 2024)
Bug: https://jira.mariadb.org/browse/MDEV-32975
From: Alexander Barkov <bar@mariadb.com>
Date: Fri, 26 Jan 2024 13:12:03 +0400
Subject: [PATCH] MDEV-32975 Default charset doesn't work with PHP MySQLi
 extension

When sending the server default collation ID to the client
in the handshake packet, translate a 2-byte collation ID
to the ID of the default collation for the character set.
---
 sql/sql_acl.cc | 21 ++++++++++++++++++++-
 1 file changed, 20 insertions(+), 1 deletion(-)

--- a/sql/sql_acl.cc
+++ b/sql/sql_acl.cc
@@ -13354,8 +13354,27 @@ static bool send_server_handshake_packet
   *end++= 0;
 
   int2store(end, thd->client_capabilities);
+
+  CHARSET_INFO *handshake_cs= default_charset_info;
+  if (handshake_cs->number > 0xFF)
+  {
+    /*
+      A workaround for a 2-byte collation ID: translate it into
+      the ID of the primary collation of this character set.
+    */
+    CHARSET_INFO *cs= get_charset_by_csname(handshake_cs->cs_name.str,
+                                            MY_CS_PRIMARY, MYF(MY_WME));
+    /*
+      cs should not normally be NULL, however it may be possible
+      with a dynamic character set incorrectly defined in Index.xml.
+      For safety let's fallback to latin1 in case cs is NULL.
+    */
+    handshake_cs= cs ? cs : &my_charset_latin1;
+  }
+
   /* write server characteristics: up to 16 bytes allowed */
-  end[2]= (char) default_charset_info->number;
+  end[2]= (char) handshake_cs->number;
+
   int2store(end+3, mpvio->auth_info.thd->server_status);
   int2store(end+5, thd->client_capabilities >> 16);
   end[7]= data_len;
